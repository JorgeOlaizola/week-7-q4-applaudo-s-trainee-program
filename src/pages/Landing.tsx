import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/landing.scss';

export default function Landing() {
  return (
    <div className='landing'>
      <div className='landing-container'>
        <h1 className='landing-title'>Welcome!</h1>
        <img
          className='landing-logo'
          src='https://api.freelogodesign.org/files/99b28c6db63c40579228e27b8dbfe77c/thumb/logo_200x200.png?v=0'
          alt='logo'
        />
        <p className='landing-text'>App developed to play funny games. Try this two!</p>
        <div className='landing-links'>
          <Link className='landing-links-item' to='/colorPicker'>
            Color Picker
          </Link>
          <Link className='landing-links-item' to='/TicTacToe'>
            Tic Tac Toe
          </Link>
        </div>
      </div>
    </div>
  );
}
