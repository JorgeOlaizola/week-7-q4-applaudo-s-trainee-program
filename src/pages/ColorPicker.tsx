import React, { useState } from 'react';
import CardContainer from '../components/ColorPicker/CardContainer';
import CPButtons from '../components/ColorPicker/CPButtons';
import { useTimeMachine } from '../hooks/useTimeMachine';
import '../styles/colorPicker.scss';

export default function ColorPicker() {
  const [colorPicker, setColorPicked, changePosition, stateInfo] = useTimeMachine<[] | number>([]);

  const [error, setError] = useState<string>('');

  const allowSelect = (id: number) => {
    if (stateInfo.isCurrent) return setColorPicked(id);
    setError('You cant pick colors while traveling in the time. Press the "Resume" button if you want to pick another color');
    return undefined;
  };

  return (
    <div className='color-picker'>
      <CardContainer
        allowSelect={allowSelect}
        memory={colorPicker}
        error={error}
        setError={setError}
      />
      <CPButtons changePosition={changePosition} stateInfo={stateInfo} />
    </div>
  );
}
