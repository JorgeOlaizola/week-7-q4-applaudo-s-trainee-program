import React, { useState, useEffect } from 'react';
import Buttons from '../components/TicTacToe/TTTButtons';
import TicTacToeContainer from '../components/TicTacToe/TTTContainer';
import { useTimeMachine } from '../hooks/useTimeMachine';
import { turn, wombType } from '../interfaces/TicTacToe';
import { ActionType } from '../interfaces/TimeMachine';
import '../styles/ticTacToe.scss';

const firstGame = () => {
  const random = Math.round(Math.random());
  if (random === 1) return 'circle';
  return 'cross';
};

export default function TicTacToe() {
  const [turn, setTurn] = useState<turn>(firstGame());
  const initialState = ['', '', '', '', '', '', '', '', ''];
  const [womb, changeWomb, changeView, stateInfo, resetMatch] =
    useTimeMachine<wombType>(initialState);
  const [game, setGame] = useState<boolean>(true);
  const [winner, setWinner] = useState<turn>('');
  const [error, setError] = useState<string>('');

  const turnHandler = () => {
    setTurn((prevTurn) => {
      if (prevTurn === 'circle') return 'cross';
      return 'circle';
    });
  };

  const timeMachine = (action: ActionType) => {
    if (!game) {
      changeView(action);
    }
  };

  const resetGame = () => {
    setGame(true);
    setWinner('');
    changeView('reset');
    setError('');
    resetMatch();
  };

  useEffect(() => matchHandler(), [turn]);

  const gameFinishedActions = () => {
    setGame(false);
    setWinner(turn === 'circle' ? 'cross' : 'circle');
  };

  const matchHandler = () => {
    const [one, two, three, four, five, six, seven, eight, nine] = womb;
    if (one !== '') {
      if (one === two && one === three) gameFinishedActions();
      if (one === four && one === seven) gameFinishedActions();
      if (one === five && one === nine) gameFinishedActions();
    }
    if (two !== '') {
      if (two === five && two === eight) gameFinishedActions();
    }
    if (three !== '') {
      if (three === six && three === nine) gameFinishedActions();
      if (three === five && three === seven) gameFinishedActions();
    }
    if (four !== '') {
      if (four === five && four === six) gameFinishedActions();
    }
    if (seven !== '') {
      if (seven === eight && seven === nine) gameFinishedActions();
    }
  };

  const wombHandler = (id: number) => {
    if (game) {
      if (womb[id] === '') {
        const newArray = [...womb];
        newArray[id] = turn;
        changeWomb(newArray);
      }
      turnHandler();
      return undefined;
    }
    setError('The game has finished. If you want to continue playing, press the "Restart" button ');
    return undefined;
  };

  return (
    <div className='ticTacToe'>
      <TicTacToeContainer
        turn={turn}
        womb={womb}
        wombHandler={wombHandler}
        winner={winner}
        error={error}
        setError={setError}
      />
      <Buttons
        turn={turn}
        game={game}
        resetGame={resetGame}
        timeMachine={timeMachine}
        stateInfo={stateInfo}
      />
    </div>
  );
}
