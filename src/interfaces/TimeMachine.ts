export interface IStateInfo {
  isCurrent: boolean;
  isLastOne: boolean;
}

export type ActionType = 'previous' | 'next' | 'reset' | 'start';
export type ChangePositionType = (action: ActionType) => void

export type TimeMachineReturnType<T> = [
  T,
  (item: T) => void,
  ChangePositionType,
  IStateInfo,
  () => void
];
