import { useState } from 'react';
import { IStateInfo, ActionType, TimeMachineReturnType } from '../interfaces/TimeMachine';

export const useTimeMachine = <T>(stateToTrack: T): TimeMachineReturnType<T> => {
  const [memory, setMemory] = useState<T[]>([stateToTrack]);
  const [pointer, setPointer] = useState<number>(0);
  const isCurrent: boolean = pointer === 0;
  const isLastOne: boolean = pointer === memory.length - 1;

  const useMemory = (item: T) => {
    setMemory((prevState: T[]) => [item, ...prevState]);
  };

  const changePosition = (action: ActionType) => {
    if (action === 'previous' && pointer < memory.length - 1) setPointer(pointer + 1);
    if (action === 'next' && pointer > 0) setPointer(pointer - 1);
    if (action === 'reset') setPointer(0);
    if (action === 'start') setPointer(memory.length - 1);
  };

  const resetTimeMachine = () => {
    setMemory([stateToTrack]);
  };

  const stateInfo: IStateInfo = {
    isCurrent,
    isLastOne,
  };

  return [memory[pointer], useMemory, changePosition, stateInfo, resetTimeMachine];
};
