import React from 'react';
import { Route } from 'react-router-dom';
import { routes } from './utils/routes';
import Footer from './components/Footer/Footer';
import Navbar from './components/Navbar/Navbar';
import ColorPicker from './pages/ColorPicker';
import Landing from './pages/Landing';
import TicTacToe from './pages/TicTacToe';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Route exact path={routes.landing} render={() => <Landing />} />
      <Route exact path={routes.colorPicker} render={() => <ColorPicker />} />
      <Route exact path={routes.ticTacToe} render={() => <TicTacToe />} />
      <Footer />
    </div>
  );
}

export default App;
