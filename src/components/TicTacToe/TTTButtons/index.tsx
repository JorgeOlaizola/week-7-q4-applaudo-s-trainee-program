import React from 'react';
import { turn } from '../../../interfaces/TicTacToe';
import { ActionType, IStateInfo } from '../../../interfaces/TimeMachine';
import Circle from '../Box/Circle';
import Cross from '../Box/Cross';
import './tttbuttons.scss';

export default function Buttons({
  turn,
  game,
  resetGame,
  stateInfo,
  timeMachine,
}: {
  turn: turn;
  game: boolean;
  resetGame: () => void;
  stateInfo: IStateInfo;
  timeMachine: (action: ActionType) => void;
}) {
  return (
    <div className='ttt-button-container'>
      <button
        onClick={() => timeMachine('next')}
        className={`ttt-button ${game || stateInfo.isCurrent ? 'ttt-button-disabled' : ''}`}
        type='button'
      >
        Next
      </button>
      <button
        onClick={() => {
          timeMachine('reset');
        }}
        className={`ttt-button ${game ? 'ttt-button-disabled' : ''}`}
        type='button'
      >
        Resume
      </button>
      <button
        onClick={() => timeMachine('previous')}
        className={`ttt-button ${game || stateInfo.isLastOne ? 'ttt-button-disabled' : ''}`}
        type='button'
      >
        Previous
      </button>
      <span className='turn-title'>Next to move</span>
      <div className='box-filled'>
        {turn === 'cross' && <Cross />}
        {turn === 'circle' && <Circle />}
      </div>
      <button onClick={resetGame} className='ttt-button' type='button'>
        Restart
      </button>
    </div>
  );
}
