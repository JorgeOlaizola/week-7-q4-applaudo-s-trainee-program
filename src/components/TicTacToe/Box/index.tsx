import React, { useState } from 'react';
import { turn, wombType } from '../../../interfaces/TicTacToe';
import './box.scss';
import Circle from './Circle';
import Cross from './Cross';

export default function Box({
  turn,
  womb,
  id,
  wombHandler
}: {
  turn: turn;
  womb: wombType
  id: number
  wombHandler: (id: number) => void;
}) {
  const contentSetter = () => {
    if (womb[id] === '') {
      wombHandler(id);
    }
  };

  return (
    <div className={womb[id] === '' ? 'box' : 'box-filled'} onClick={contentSetter}>
      {womb[id] === 'cross' && <Cross />}
      {womb[id] === 'circle' && <Circle />}
    </div>
  );
}
