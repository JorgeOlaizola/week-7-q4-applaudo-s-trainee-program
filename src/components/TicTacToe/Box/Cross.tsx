import React from 'react';
import './box.scss';

export default function Cross() {
  return (
    <div>
      <div className='box-cross box-cross-left'> </div>
      <div className='box-cross box-cross-right'> </div>
    </div>
  );
}
