import React, { Dispatch, SetStateAction, useState } from 'react';
import { turn, wombType } from '../../../interfaces/TicTacToe';
import Box from '../Box';
import './tttcontainer.scss';

export default function TicTacToeContainer({
  turn,
  womb,
  wombHandler,
  winner,
  error,
  setError,
}: {
  turn: turn;
  womb: wombType;
  wombHandler: (id: number) => void;
  winner: turn;
  error: string;
  setError: Dispatch<SetStateAction<string>>;
}) {
  let id = 0;
  return (
    <div>
      {winner && (
        <h2 className='ttt-winner'>
          The winner is <span className='ttt-winner-side'>{winner}</span>
        </h2>
      )}
      {error && (
        <h3 className='ttt-error'>
          {error}
          <button className='ttt-error-button' type='button' onClick={() => setError('')}>
            X
          </button>
        </h3>
      )}
      <div className='ttt-container'>
        {womb.map(() => (
          <Box turn={turn} id={id++} womb={womb} wombHandler={wombHandler} />
        ))}
      </div>
    </div>
  );
}
