import React, { useEffect } from 'react';
import './card.scss';

export default function Card({
  color,
  id,
  allowSelect,
  memory
}: {
  color: string;
  id: number;
  allowSelect: (id: number) => void;
  memory: [] | number;
}) {
  let cardElement;

  useEffect(() => {
    cardElement = document.getElementById(`${id}`)!;
    cardElement.style.backgroundColor = color;
  }, []);
  useEffect(() => {
    cardElement = document.getElementById(`${id}`)!;
    if (memory === id) {
      cardElement.className = 'card card-selected';
    } else cardElement.className = 'card';
  }, [memory]);
  return <div id={`${id}`} onClick={() => allowSelect(id)} className="card" />;
}
