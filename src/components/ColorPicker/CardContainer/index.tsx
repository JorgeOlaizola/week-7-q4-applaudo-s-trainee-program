import React, { Dispatch, SetStateAction } from 'react';
import Card from '../Card';
import { colors } from '../../../utils/colors';
import './cardContainer.scss';

export default function CardContainer({
  allowSelect,
  memory,
  error,
  setError
}: {
  allowSelect: (id: number) => void;
  memory: [] | number;
  error: string;
  setError: Dispatch<SetStateAction<string>>
}) {
  let id = 1;
  return (
    <div className='color-picker-container'>
      { error && <div className='color-picker-error'>{error}<button type='button' className='color-picker-error-button' onClick={() => setError('')}>X</button></div> }
      <div className='card-container'>
        {colors.map((color) => (
          <Card color={color} id={id++} allowSelect={allowSelect} memory={memory} />
        ))}
      </div>
    </div>
  );
}
