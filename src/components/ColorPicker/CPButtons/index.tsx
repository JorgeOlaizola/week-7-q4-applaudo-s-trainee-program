import React from 'react';
import { ChangePositionType, IStateInfo } from '../../../interfaces/TimeMachine';
import './cpButtons.scss';

export default function Buttons({
  changePosition,
  stateInfo,
}: {
  changePosition: ChangePositionType;
  stateInfo: IStateInfo;
}) {
  return (
    <div className='picker-button-container'>
      <button
        className={`picker-button ${stateInfo.isCurrent ? 'picker-button-disabled' : ''}`}
        onClick={() => changePosition('next')}
        type='button'
      >
        Next
      </button>
      <button
        className={`picker-button ${stateInfo.isCurrent ? 'picker-button-disabled' : ''}`}
        onClick={() => changePosition('reset')}
        type='button'
      >
        Resume
      </button>
      <button
        className={`picker-button ${stateInfo.isLastOne ? 'picker-button-disabled' : ''}`}
        onClick={() => changePosition('previous')}
        type='button'
      >
        Previous
      </button>
    </div>
  );
}
