import React from 'react';
import './footer.scss';

export default function Footer() {
  return <footer className="footer">App developed by Jorge Olaizola. Make sure to follow me on&nbsp;<a href='https://gitlab.com/JorgeOlaizola'>GitLab</a></footer>;
}
