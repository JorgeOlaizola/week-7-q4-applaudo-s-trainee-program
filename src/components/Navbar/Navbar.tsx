import React from 'react';
import './navbar.scss';
import { Link } from 'react-router-dom';

export default function Navbar() {
  return (
    <nav className='nav'>
      <Link to='/'>
        <img className='nav-logo' src='https://api.freelogodesign.org/files/99b28c6db63c40579228e27b8dbfe77c/thumb/logo_200x200.png?v=0' alt='logo' />
      </Link>
      <div className='nav-links'>
        <Link className='nav-links-item' to='/colorPicker'>
          Color Picker
        </Link>
        <Link className='nav-links-item' to='/TicTacToe'>
          Tic Tac Toe
        </Link>
      </div>
    </nav>
  );
}
