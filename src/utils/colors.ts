export const colors = [
  'maroon',
  'skyblue',
  'yellow',
  'navy',
  'orange',
  'blue',
  'chartreuse',
  'crimson',
  'darkslategrey',
  'forestgreen',
  'gold',
  'indigo',
  'lightcoral',
  'lightseagreen',
  'palevioletred',
  'tomato',
];
